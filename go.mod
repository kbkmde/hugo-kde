module invent.kde.org/websites/hugo-kde

go 1.14

require (
	github.com/PhuNH/hg-companion v0.0.0-20230913110054-b505a89c6dad // indirect
	github.com/thednp/bootstrap.native v0.0.0-20230704154857-7778c4403d89 // indirect
	invent.kde.org/websites/hugo-bootstrap v0.0.0-20230310220713-2c58a2e72e20 // indirect
)
